# DiscordModNotif Bot
A very basic discord bot that references chat messages that match a pattern (default is mod and admin in all cases and variations)

## Dependencies
- discord.py (`pip3 install discord` or `sudo apt get install python3-discord`)

## How to run
python3 /path/to/DiscordModNotif.py channel-id bot-token

- channel-id: the channel which the bot will reference the messages in
- bot-token: your discord bot token

## Notes
- Users with 'admin' role are ignored
