"""
MIT License

Copyright (c) 2023 DarkBlue

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

#!/usr/bin/env python3

import re
import sys
import discord

if len(sys.argv) != 3:
    sys.exit("Usage: DiscordModNotif.py OUTPUT_CHANNEL_ID BOT_TOKEN")

CHANNEL_ID = int(sys.argv[1])

class DiscordModNotif(discord.Client):
    async def on_ready(self):
        print(f'Logged in as {self.user} (ID: {self.user.id})')
        channel = client.get_channel(CHANNEL_ID)
        await channel.send('**:robot: DiscordModNotif bot connected**')

    async def on_message(self, message):
        # we do not want the bot to reply to itself
        if message.author.id == self.user.id:
            return
        if message.channel.id == CHANNEL_ID:
            return
        for userRole in message.author.roles:
            if userRole.name.lower() == "admin":
                return

        matchedContent = re.search(r"\bmod|\badmin", message.content, re.IGNORECASE)
        if matchedContent:
            channel = client.get_channel(CHANNEL_ID)
            await channel.send(f'{message.jump_url}\nMatches: `{matchedContent.group()}`\nUser: `{message.author.name}`\nText: `{message.content}`')

intents = discord.Intents.default()
intents.message_content = True
client = DiscordModNotif(intents=intents)
client.run(sys.argv[2])
